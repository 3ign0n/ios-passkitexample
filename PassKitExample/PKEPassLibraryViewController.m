//
//  PKESecondViewController.m
//  PassKitExample
//
//  Created by Takeda Hiroyuki(aka @3ign0n) on 12/09/21.
//
#import <PassKit/PKPassLibrary.h>
#import "PKPass+Debug.h"
#import "PKEPassLibraryViewController.h"

@interface PKEPassLibraryViewController ()

@end

@implementation PKEPassLibraryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCheckIfPassLibraryIsAvairableDidTapped:(id)sender {
    UIAlertView* alert  = [[UIAlertView alloc] initWithTitle:@"INFO" message:[NSString stringWithFormat:@"Pass library is %@available", [PKPassLibrary isPassLibraryAvailable] ? @"" : @"un"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)btnGetPassesFromLibraryDidTapped:(id)sender {
    if ([PKPassLibrary isPassLibraryAvailable]) {
        PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
        NSArray* passes = [passLib passes];
        for (PKPass* pass in passes) {
            [pass dump];
        }
    }
}


@end
