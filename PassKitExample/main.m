//
//  main.m
//  PassKitExample
//
//  Created by Takeda Hiroyuki(aka @3ign0n) on 12/09/21.
//

#import <UIKit/UIKit.h>

#import "PKEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PKEAppDelegate class]));
    }
}
