//
//  PKEFirstViewController.m
//  PassKitExample
//
//  Created by Takeda Hiroyuki(aka @3ign0n) on 12/09/21.
//
#import <PassKit/PKPass.h>
#import <PassKit/PKAddPassesViewController.h>
#import <PassKit/PKPassLibrary.h>
#import "PKPass+Debug.h"
#import "PKEAddPassViewController.h"

@interface PKEAddPassViewController ()<PKAddPassesViewControllerDelegate> {
}
@property(nonatomic, strong) NSMutableData* receivedData;
@property(nonatomic, strong) NSString* cacheFileName;
@property(nonatomic, strong) UIView *indicatorBackgroundView;
@property(nonatomic, strong) UIActivityIndicatorView *indicator;
@property(nonatomic, strong) PKPass* currentPass;

- (void)startGettingPassWithUrl:(NSString*)urlString;
- (NSString*)extractCacheFileName:(NSString*)urlString;
- (NSData*)loadPassFromCache:(NSString*)fileName;
- (void)addPass:(NSData*)passData;
- (void)dismissAlertView:(NSTimer*)timer;
- (void)startIndicator;
- (void)stopIndicator;
- (void)closeKeyboard:(UITextField*)textField;
@end

@implementation PKEAddPassViewController
@synthesize txtfldUrl;
@synthesize receivedData;
@synthesize cacheFileName;
@synthesize indicatorBackgroundView;
@synthesize indicator;
@synthesize currentPass;

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    receivedData = [[NSMutableData alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnGetThePassDidTapped:(id)sender {
    [self closeKeyboard:txtfldUrl];
    [self startGettingPassWithUrl:[txtfldUrl text]];
}

#pragma mark - private

- (void)startGettingPassWithUrl:(NSString*)urlString {
    [self startIndicator];
    
    // for caching pass data
    cacheFileName = [self extractCacheFileName:urlString];
    NSLog(@"cache 0x%08x", (int)cacheFileName);
    
    NSData* passData = nil;
    if (cacheFileName) {
        passData = [self loadPassFromCache:cacheFileName];
    }
    
    if (passData) {
        [self stopIndicator];
        [self addPass:passData];
    } else {
        // get the pass from remote host
        NSURL* url = [NSURL URLWithString:urlString];
        NSURLRequest* req = [NSURLRequest requestWithURL:url];
        NSURLConnection* con = [NSURLConnection connectionWithRequest:req delegate:self];
        if (!con) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"something wrong in NSURLConnection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
}

#pragma mark - NSURLConnection callbacks
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    UIApplication* app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = NO;
    [self stopIndicator];
    
    // inform the user
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"ERROR" message:[NSString stringWithFormat:@"didFailWithError, %@", [error localizedDescription]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self cachePass:receivedData];
    
    UIApplication* app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = NO;
    [self stopIndicator];
    
    [self addPass:receivedData];
    receivedData = nil;
}

#pragma mark - caching passes
- (NSString*)extractCacheFileName:(NSString*)urlString {
    NSString* file = nil;
    NSRange slashRange = [urlString rangeOfString:@"/" options:NSBackwardsSearch];
    if (slashRange.location != NSNotFound) {
        file = [urlString substringFromIndex:slashRange.location+1];
    }
    return file;
}

- (NSData*)loadPassFromCache:(NSString*)fileName {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [paths objectAtIndex:0];
    cachePath = [cachePath stringByAppendingPathComponent:fileName];
    return [NSData dataWithContentsOfFile:cachePath];
}

- (void)cachePass:(NSData*)passData {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [paths objectAtIndex:0];
    cachePath = [cachePath stringByAppendingPathComponent:cacheFileName];
    [receivedData writeToFile:cachePath atomically:YES];
    cacheFileName = nil;
}

- (void)addPass:(NSData*)passData {
    NSError* error = nil;
    currentPass = [[PKPass alloc] initWithData:passData error:&error];
    if (error) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:[NSString stringWithFormat:@"PKPass init failed, %@", [error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    // for debug
    [currentPass dump];
    
    PKAddPassesViewController* passView = [[PKAddPassesViewController alloc] initWithPass:currentPass];
    passView.delegate = self;
    
    // presentModalViewController:animated: is deprecated in iOS6.0
    [self presentViewController:passView animated:YES completion:^{
        NSLog(@"on completion");
    }];
}

#pragma mark - PKAddPassesView callbacks
#define KEY_DISMISS_TARGET @"dismissTarget"
-(void)addPassesViewControllerDidFinish:(PKAddPassesViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"passView dismissed");
    }];
    
    // Hey! How can we detect if a user press "Cancel" button or "Add" button,
    // preferably the pass was added to passbook successfully or not?
    // You can use -[PKPassLibrary containsPass:], but are there any better ways?
    PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
    NSString* msg;
    if ([passLib containsPass:currentPass]) {
        msg = @"The pass is successfully added to passbook";
    } else {
        msg = @"adding the pass is canceled";
    }
    currentPass = nil;
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"INFO" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    
    NSDictionary* userInfo = [NSDictionary dictionaryWithObject:alert forKey:KEY_DISMISS_TARGET];
    [NSTimer scheduledTimerWithTimeInterval:1.2f target:self selector:@selector(dismissAlertView:) userInfo:userInfo repeats:NO];
}

-(void)dismissAlertView:(NSTimer*)timer {
    NSDictionary* userInfo = [timer userInfo];
    UIAlertView* alert = [userInfo objectForKey:KEY_DISMISS_TARGET];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - Indicator
- (void)startIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
    indicatorBackgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    indicatorBackgroundView.backgroundColor = [UIColor blackColor];
    indicatorBackgroundView.alpha = 0.5f;
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge ];
    [indicator setCenter:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2)];
    [indicatorBackgroundView addSubview:indicator];
    [self.view addSubview:indicatorBackgroundView];
    [indicator startAnimating];
}

- (void)stopIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [indicator stopAnimating];
    [indicatorBackgroundView removeFromSuperview];
}

#pragma mark - keyboard
- (IBAction)txtfldUrlDidEndOnExit:(id)sender {
    [self closeKeyboard:(UITextField*)sender];
}

- (void)closeKeyboard:(UITextField*)textField {
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
}
@end
