//
//  PKPass+Debug.h
//  PassKitExample
//
//  Created by Takeda Hiroyuki(aka @3ign0n) on 12/09/23.
//

#import <Foundation/Foundation.h>
#import <PassKit/PassKit.h>

@interface PKPass(Debug)
- (void)dump;
@end
