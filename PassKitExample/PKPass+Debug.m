//
//  PKPass+Debug.m
//  PassKitExample
//
//  Created by Takeda Hiroyuki(aka @3ign0n) on 12/09/23.
//

#import "PKPass+Debug.h"

@implementation PKPass(Debug)

- (void)dump {
    NSLog(@"passURL:%@", [[self passURL] path]);
    NSLog(@"authenticationToken:%@", [self authenticationToken]);
    NSLog(@"passTypeIdentifier:%@", [self passTypeIdentifier]);
    NSLog(@"serialNumber:%@", [self serialNumber]);
    // skipping [pass icon]
    NSLog(@"localizedName:%@", [self localizedName]);
    NSLog(@"localizedDescription:%@", [self localizedDescription]);
    // skipping [pass localizedValueForFieldKey:(NSString*)]
    NSLog(@"organizationName:%@", [self organizationName]);
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"yyyy/MM/dd" options:0 locale:[NSLocale currentLocale]]];
    NSLog(@"relevantDate:%@", [dateFormatter stringFromDate:[self relevantDate]]);
    
    NSLog(@"webServiceURL:%@", [[self webServiceURL] path]);
}

@end
